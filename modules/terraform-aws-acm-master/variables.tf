variable "domain_name" {
  default = "tapinlu.epizy.com"
}

variable "validation_method" {
  default = "DNS"
}

variable "validation_domain" {
  default = "epizy.com"
}