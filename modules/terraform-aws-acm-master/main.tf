#AWS Certificate Manager

resource "aws_acm_certificate" "acm-ceritificate" {
  domain_name = var.domain_name
  validation_method = var.validation_method

  validation_option {
    domain_name = var.domain_name
    validation_domain = var.validation_domain
  }
}