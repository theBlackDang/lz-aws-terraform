resource "aws_vpn_gateway" "vpn-gateway" {
  

  # vpn_gateway_id          = module.vpc.vgw_id
  # customer_gateway_id     = aws_customer_gateway.main.id
}

data "http" "my_ip" {
  url = "http://ipv4.icanhazip.com"
}

resource "aws_customer_gateway" "main" {
  bgp_asn = 650000
   ip_address = "${(chomp(data.http.my_ip.body))}"
   type = "ipsec.1"
}