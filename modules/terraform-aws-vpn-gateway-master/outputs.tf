data "http" "icanhazip" {
  url = "http://icanhazip.com"
}


output "public_ip" {
  value = "${chomp(data.http.icanhazip.body)}"
}