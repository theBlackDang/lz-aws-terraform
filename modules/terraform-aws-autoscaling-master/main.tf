resource "aws_placement_group" "placement_group" {
  name     = "placement_group"
  strategy = "cluster"
}

resource "aws_autoscaling_group" "autoscaling" {
  name = "autoscaling"
  max_size = 1
  min_size = 3
  desired_capacity = 1
  force_delete = true
  placement_group = aws_placement_group.placement_group.id
  target_group_arns = ["${module.alb.aws_lb_target_group.target_group.arn}"]
  launch_configuration = module.ec2.aws_launch_configuration.ec2_launch_configuration.name
  vpc_zone_identifier = [module.vpc.aws_subnet.private_subnet_id_1a, module.vpc.aws_subnet.private_subnet_id_1a]
}

module "vpc" {
  source = "../terraform-aws-vpc-master"
}

module "ec2" {
  source = "../terraform-aws-ec2-instance-master"
}

module "alb" {
  source = "../terraform-aws-alb-master"
}



