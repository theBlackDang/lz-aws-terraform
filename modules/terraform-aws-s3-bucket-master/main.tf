resource "aws_s3_bucket" "s3_bucket" {
  bucket = "s3-bucket"
}

resource "aws_s3_bucket_acl" "aws_s3_bucket_acl" {
  bucket = aws_s3_bucket.s3_bucket.id
  acl = "private"
}