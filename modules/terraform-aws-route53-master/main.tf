resource "aws_route53_zone" "primary" {
  name = "epizy.com"
}


resource "aws_route53_zone" "sub" {
  name = "tapinlu.epizy.com"
}

resource "aws_route53_record" "www" {
  zone_id = aws_route53_zone.primary.zone_id
  name = "www.epizy.com"
  type = "A"
  ttl = 300
  records = [module.alb.alb_id]
}

module "alb" {
  source = "../terraform-aws-alb-master"
}