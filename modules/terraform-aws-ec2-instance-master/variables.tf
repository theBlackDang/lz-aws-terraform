variable "ec2_name" {
  default = "ec2-micro"
}

variable "ec2_ami" {
  default = "ami-ebd02392"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "key_name" {
  default = "EC2-keypair"
}
