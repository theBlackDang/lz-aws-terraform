output "ec2_1a_id" {
  value = aws_instance.ec2_1a.id
}

output "ec2_1b_id" {
  value = aws_instance.ec2_1b.id
}