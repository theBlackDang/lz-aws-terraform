locals {
  region = "ap-southeast-1"
}


#EC2 1a
resource "aws_instance" "ec2_1a" {
#  name = "var.ec2_name-${each.key}"
  ami                    = var.ec2_ami
  instance_type          = var.instance_type
  key_name               = var.key_name
  monitoring             = true
#  vpc_security_group_ids = [aws_security_group.security.id]
   subnet_id = module.vpc.private_subnet_id_1a
}

#EC2 1b
resource "aws_instance" "ec2_1b" {
  ami                    = var.ec2_ami
  instance_type          = var.instance_type
  key_name               = var.key_name
  monitoring             = true
#  vpc_security_group_ids = [aws_security_group.security.id]
   subnet_id = module.vpc.private_subnet_id_1b
}

resource "aws_ebs_volume" "ebs_volume_1a" {
  availability_zone = "${local.region}a"
  size              = 1
  encrypted         = true
}

resource "aws_ebs_volume" "ebs_volume_1b" {
  availability_zone = "${local.region}b"
  size              = 1
  encrypted         = true
}

resource "aws_volume_attachment" "ebs_att_1a" {
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.ebs_volume_1a.id
  instance_id = aws_instance.ec2_1a.id
}

resource "aws_volume_attachment" "ebs_att_1b" {
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.ebs_volume_1b.id
  instance_id = aws_instance.ec2_1b.id
}

resource "aws_launch_configuration" "ec2_launch_configuration" {
  name_prefix     = "ec2_launch_configuration"
  image_id        = var.ec2_ami
  instance_type   = var.instance_type
#  user_data       = file("user-data.sh")
#  security_groups = [module.vpc.security_group.sg.id]

  lifecycle {
    create_before_destroy = true
  }
}


module "vpc" {
  source = "../terraform-aws-vpc-master"
}