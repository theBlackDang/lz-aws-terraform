output "alb_id" {
  value = aws_lb.application_load_balancer.dns_name
}

output "alb_arn" {
  value = aws_lb.application_load_balancer.arn
}