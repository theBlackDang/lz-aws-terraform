resource "aws_lb" "application_load_balancer" {
  name = "ApplicationLoadBalancer"
  internal = false 
  load_balancer_type = "application"
  security_groups = module.vpc.vpc_security_group_id.*

  subnet_mapping {
    subnet_id = module.vpc.private_subnet_id_1a
    allocation_id = aws_eip.application_load_balancer_eip.id
  }
}

resource "aws_lb_target_group" "target_group" {
  name = "TargetGroup"
  port = 80
  protocol = "HTTP"
  vpc_id = module.vpc.vpc_id
}


resource "aws_lb_listener" "listener" {
  load_balancer_arn = aws_lb.application_load_balancer.arn
  port = "80"
  protocol = "HTTP"

  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.target_group.arn
  }
}


resource "aws_lb_target_group_attachment" "target_group_attachment" {
  target_group_arn = aws_lb_target_group.target_group.arn
  target_id = module.ec2.ec2_1a_id
}

resource "aws_eip" "application_load_balancer_eip" {
tags = {
  "Name" = "ApplicationLoadBalancerElasticIP"
}
}


module "vpc" {
  source = "../terraform-aws-vpc-master"
}

module "ec2" {
  source = "../terraform-aws-ec2-instance-master"
}