locals {
  region = "ap-southeast-1"
}


#VPC
resource "aws_vpc" "vpc" {

  cidr_block = var.cidr
  enable_dns_hostnames = true 

  tags = {
    "Name" = "Main VPC"
  }
}

#Elastic IP for NAT
resource "aws_eip" "elastic-ip" {
  vpc = true 
}



# resource "aws_network_interface" "multi-ip" {
#   subnet_id   = aws_subnet.public_subnet_1a
#   private_ips = ["10.0.0.10", "10.0.0.11"]
# }

#Public Subnet 1a
resource "aws_subnet" "public_subnet_1a" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = "10.0.0.0/24"
  availability_zone = "${local.region}a"
  depends_on = [aws_internet_gateway.internet_gateway]

  tags = {
    "Name" = "Public subnet 1a"
  }
}


#Private Subnet 1a
resource "aws_subnet" "private_subnet_1a" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "${local.region}a"
  tags = {
    "Name" = "Private subnet"
  }
}

#Public Subnet 1b
resource "aws_subnet" "public_subnet_1b" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = "10.0.0.0/24"
  availability_zone = "${local.region}b"
  depends_on = [aws_internet_gateway.internet_gateway]
  tags = {
    "Name" = "Public subnet 1b"
  }
}

#Private Subnet 1b
resource "aws_subnet" "private_subnet_1b" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "${local.region}b"

  tags = {
    "Name" = "Private subnet 1b"
  }
}



# NAT Gateway 1a
resource "aws_nat_gateway" "nat-gateway_1a" {
  allocation_id = aws_eip.elastic-ip.id
  subnet_id     = aws_subnet.public_subnet_1a.id
}

# NAT Gateway 1b
resource "aws_nat_gateway" "nat-gateway_1b" {
  allocation_id = aws_eip.elastic-ip.id
  subnet_id     = aws_subnet.public_subnet_1b.id
}


resource "aws_security_group" "sg" {

  #Allow Transport Layer Security - Layer 4 in OSI Model
  name = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id = aws_vpc.vpc.id

  # ingress = {
  #   cidr_block = [aws_vpc.vpc.cidr_block]
  #   description = "Ingress"
  #   from_port = 443
  #   to_port = 443
  #   protocol = "tcp"


  # } 

  # egress = {
  #     from_port = 0
  #     to_port = 0
  #     protocol = "-1"
  #     cidr_blocks = ["0.0.0.0/0"]
  # }

  tags = {
    "Name" = "allow_tls"
  }

}

resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = aws_vpc.vpc.id
}




resource "aws_network_interface" "network_interface_1a" {
  subnet_id = aws_subnet.private_subnet_1a.id
  private_ips = ["10.0.0.50"]
  security_groups = [aws_security_group.sg.id]

  # attachment {
  #   instance = module.ec2.ec2_1a_id
  # }
}


# module "ec2" {
#   source = "../terraform-aws-ec2-instance-master"
# }


