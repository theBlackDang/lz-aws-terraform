# Value equals resource type multiply variable of that resource 

output "vpc_id" {
  value = aws_vpc.vpc.id
}

output "private_subnet_id_1a" {
  value = aws_subnet.private_subnet_1a.id
}

output "private_subnet_id_1b" {
  value = aws_subnet.private_subnet_1b.id
}

output "public_subnet_id_1a" {
  value = aws_subnet.public_subnet_1a.id
}

output "public_subnet_id_1b" {
  value = aws_subnet.public_subnet_1b.id
}

output "vpc_security_group_id" {
  value = aws_security_group.sg.id
}

output "databse_subnet_id_1a" {
  value = aws_subnet.private_subnet_1a.id
}

output "databse_subnet_id_1b" {
  value = aws_subnet.private_subnet_1b.id
}

output "aws_nat_gateway_1a" {
  value = aws_nat_gateway.nat-gateway_1a
}

output "aws_nat_gateway_1b" {
  value = aws_nat_gateway.nat-gateway_1b
}