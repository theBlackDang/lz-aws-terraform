variable "vpc_name" {
  default = "vpc"
}

variable "cidr" {
  default = "10.0.0.0/16"
}

variable "availability_zones" {
  default = ["ap-southeast-1", "ap-southeast-2", "ap-southeast-3"]
}

variable "private_subnets" {
  default = ["10.0.1.0/24","10.0.2.0/24","10.0.3.0/24"]
}
variable "database_subnets" {
  default = ["10.0.21.0/24", "10.0.22.0/24", "10.0.23.0/24"]
}

variable "public_subnets" {
  default = ["10.0.101.0/24","10.0.102.0/24","10.0.103.0/24"]
}

