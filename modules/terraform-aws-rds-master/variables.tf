variable "db_identifier" {
  default = "demodb"
}

variable "db_engine" {
  default = "mysql"
}

variable "engine_version" {
  default = "5.7.25"
}

variable "instance_class" {
  default = "db.t2.micro"
}

variable "db_name" {
  default = "free-mysql-t2.micro"
}

variable "db_username" {
  default = "haidang"
}

variable "db_port" {
  default = "3306"
}

variable "maintenance_window" {
  default = "Mon:00:00-Mon:03:00"
}

variable "backup_window" {
  default = "03:00-06:00"
}

variable "db_parameter_group" {
  default = "mysql5.7"
}

variable "db_option_group" {
  default = "5.7"
}

