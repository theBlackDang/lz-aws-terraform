#Require 2 database instance, primary and backup
resource "aws_db_instance" "primary" {
  db_name              = var.db_identifier
  engine               = var.db_engine
  engine_version       = var.engine_version
  instance_class       = var.instance_class
  username             = var.db_username
  parameter_group_name = var.db_parameter_group
  skip_final_snapshot  = true
  vpc_security_group_ids = [module.vpc.vpc_security_group_id]
  
}

#Database subnet 1a
resource "aws_db_subnet_group" "database_subnet_group_1a" {
  name = "database subnet group 1a"
  subnet_ids = [module.vpc.databse_subnet_id_1a]
}

#Database subnet 1b
resource "aws_db_subnet_group" "database_subnet_group_1b" {
  name = "database subnet group 1b"
  subnet_ids = [module.vpc.databse_subnet_id_1b]
}


module "vpc" {
  source = "../terraform-aws-vpc-master"
}