variable "waf_rule_name" {
  default = "Web Application Firewall"
}

variable "waf_metric_name" {
  default = "WebApplicationFirewall"
}