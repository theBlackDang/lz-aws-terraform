resource "aws_waf_ipset" "ipset" {
  name = "IP Set"
  ip_set_descriptors {
    type = "IPV4"
    value = "192.0.7.0/24"
  }
}

resource "aws_waf_rule" "waf_rule" {
  depends_on = [aws_waf_ipset.ipset]
  name = var.waf_rule_name
  metric_name = var.waf_metric_name
}