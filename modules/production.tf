provider "aws"{
    region = local.region
  #  shared_credentials_files = ["C:/Users/Administrator/.aws/credentials"] 
}

locals {
  region = "ap-southeast-1"
}

module "aws-route53" {
    source = "./terraform-aws-route53-master"
}

module "cloud-front" {
  source = "./terraform-aws-cloudfront-master"
}

module "s3-bucket" {
  source = "./terraform-aws-s3-bucket-master"
}

module "aws-ceritificate-manager" {
  source = "./terraform-aws-acm-master"
}

module "aws-waf" {
  source = "./terraform-aws-waf-master"
}

module "aws-vpn-gateway" {
  source = "./terraform-aws-vpn-gateway-master"
}

module "vpc" {
  source = "./terraform-aws-vpc-master"
  availability_zones = ["${local.region}a", "${local.region}b", "${local.region}c"]
}

#EBS attached to EC2 inside EC2 module
module "ec2" {
  source = "./terraform-aws-ec2-instance-master"
}
module "elastic-load-balancing"{
  source = "./terraform-aws-alb-master"
}

module "rds" {
  source = "./terraform-aws-rds-master"
}