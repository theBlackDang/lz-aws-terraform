resource "aws_organizations_organization" "name" {
  
}

resource "aws_organizations_account" "core" {
  name = "core"
  email = "core@gmail.com"
  role_name = "Admin"
}

resource "aws_organizations_account" "test" {
  name = "test"
  email = "test@gmail.com"
  role_name = "Admin"
}

resource "aws_organizations_account" "staging" {
  name = "staging"
  email = "staging@gmail.com"
  role_name = "Admin"
}

resource "aws_organizations_account" "production" {
  name = "production"
  email = "production@gmail.com"
  role_name = "Admin"
}

resource "aws_organizations_account" "sandbox" {
  name = "sandbox"
  email = "sandbox@gmail.com"
  role_name = "Admin"
}