resource "aws_iam_group" "core_managing" {
  name = "CoreManaging"
  provider = aws.core
}

resource "aws_iam_group_policy_attachment" "core_read_only_access" {
  group = aws_iam_group.core_managing.name
  policy_arn = "arn:aws:iam:aws:policy/IAMReadOnlyAccess"
  provider = aws.core
}

resource "aws_iam_group_policy_attachment" "core_self_manage_service_specific_credentials" {
  group = aws_iam_group.core_managing.name
  policy_arn = "arn:aws:iam:aws:policy/IAMSelfManageServiceSpecificCredentials"
  provider = aws.core
}

resource "aws_iam_group_policy_attachment" "core_user_change_password" {
  group = aws_iam_group.core_managing.name
  policy_arn = "arn:aws:iam:aws:policy/IAMUserChangePassword"
  provider = aws.core
}

resource "aws_iam_policy"  "self_manage_vmfa" {
  name = "SelfManageVMFA"
  #Pre-existing policy -> error
  policy = file("${path.module}/data/self_manage_vmfa.json")
  provider = aws.core
}

resource "aws_iam_group_policy_attachment" "self_manage_vmfa" {
  group = aws_iam_group.core_managing.name
  policy_arn = aws_iam_policy.self_manage_vmfa.arn
  provider = aws.core
}