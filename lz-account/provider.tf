provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::${aws_organizations_account.core.id}:role/Admin"
  }
  alias = "core"
  region = "ap-southeast-1"
}

provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::${aws_organizations_account.test.id}:role/Admin"
  }
  alias = "test"
  region = "ap-southeast-1"
}

provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::${aws_organizations_account.staging.id}:role/Admin"
  }
  alias = "staging"
  region = "ap-southeast-1"
}

provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::${aws_organizations_account.production.id}:role/Admin"
  }
  alias = "production"
  region = "ap-southeast-1"
}

provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::${aws_organizations_account.sandbox.id}:role/Admin"
  }
  alias = "sandbox"
  region = "ap-southeast-1"
}

# resource "aws_iam_group" "core_managing" {
#   name = "CoreManaging"
#   provider = aws.core
# }

#The problem is aws_iam_group, others successfully created